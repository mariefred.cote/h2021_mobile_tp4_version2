package com.h2021.tp4_produits.model;

public class Produit {

    private String id;
    private String nom;
    private Double prix;
    private String pseudo;
    private String magasin;

    public Produit() {
    }

    public Produit(String nom, Double prix, String pseudo, String magasin) {
        this.nom = nom;
        this.prix = prix;
        this.pseudo = pseudo;
        this.magasin = magasin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMagasin() {
        return magasin;
    }

    public void setMagasin(String magasin) {
        this.magasin = magasin;
    }
}
