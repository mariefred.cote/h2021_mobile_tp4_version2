package com.h2021.tp4_produits.ui.Magasin;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.h2021.tp4_produits.R;
import com.h2021.tp4_produits.model.Magasin;

import java.util.ArrayList;
import java.util.List;

public class MagasinFragment extends Fragment {


    private List<Magasin> listMagasins;
    private ListView lvMagsins;
    private ArrayAdapter<Magasin> arrayAdapter;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_magasin, container, false);
        lvMagsins = root.findViewById(R.id.lv_magasins);
        listMagasins = new ArrayList<>();

        arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, listMagasins);

        lvMagsins.setAdapter(arrayAdapter);
        registerForContextMenu(lvMagsins);

        lvMagsins.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    NavDirections actionMagasin = MagasinFragmentDirections.actionNavMagasinToMagasinDetailFragment(listMagasins.get(position).getNom());
                    Navigation.findNavController(root).navigate(actionMagasin);

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }

            }
        });


        return root;
    }

    @Override
    public void onCreateContextMenu(@NonNull ContextMenu menu, @NonNull View v, @Nullable ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuItem delete = menu.add(0,v.getId(), 0, R.string.supprimer);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        db.collection(getString(R.string.collection_magasins)).document(listMagasins.get(menuInfo.position).getId()).delete();
        arrayAdapter.notifyDataSetChanged();
        return super.onContextItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        db.collection(getString(R.string.collection_magasins))
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        listMagasins.clear();
                        for (QueryDocumentSnapshot document : value) {
                            Magasin magasin = document.toObject(Magasin.class);
                            magasin.setId(document.getId());
                            listMagasins.add(magasin);
                        }
                        arrayAdapter.notifyDataSetChanged();
                    }
                });

    }
}