package com.h2021.tp4_produits.ui.Carte;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.h2021.tp4_produits.R;
import com.h2021.tp4_produits.model.Magasin;

public class DialogMagasin extends DialogFragment {

    private EditText etNomMagasin;
    private Double latitude;
    private Double longiude;
    private final String TAG = "tag";

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public DialogMagasin(Double latitude, Double longiude) {
        this.latitude = latitude;
        this.longiude = longiude;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_magasin, null);
        Context context = getContext();
        etNomMagasin = view.findViewById(R.id.et_nomMagasin);

        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String nomMagasin = etNomMagasin.getText().toString().trim();

                        if (nomMagasin.isEmpty()){
                            Toast.makeText(context, R.string.erreur_nom_magasin, Toast.LENGTH_LONG).show();
                            return;
                        }



                        Magasin magasin = new Magasin(nomMagasin, latitude, longiude);

                        db.collection(getString(R.string.collection_magasins))
                                .add(magasin)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        //Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //Log.w(TAG, "Error adding document", e);
                                    }
                                });
                    }
                })
                .setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        builder.setTitle(R.string.ajouer_magasin);

        return builder.create();
    }
}
