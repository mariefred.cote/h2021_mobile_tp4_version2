package com.h2021.tp4_produits.ui.Magasin;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.h2021.tp4_produits.MainActivity;
import com.h2021.tp4_produits.R;
import com.h2021.tp4_produits.model.Produit;

import java.util.ArrayList;


public class MagasinDetailFragment extends Fragment implements View.OnClickListener {

    private ArrayList<Produit> listProduits;
    private RecyclerView rvProduits;
    private ProduitAdapter produitAdapter;
    private String nomMagasin;
    private FloatingActionButton btAddProduit;
    private MediaPlayer mediaPlayer;
    private SharedPreferences prefs;

    FirebaseFirestore db = FirebaseFirestore.getInstance();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_magasin_detail, container, false);

        btAddProduit = view.findViewById(R.id.bt_add_produit);
        rvProduits = view.findViewById(R.id.rv_produits);
        listProduits = new ArrayList<>();
        produitAdapter = new ProduitAdapter(listProduits);
        rvProduits.setLayoutManager(new LinearLayoutManager(requireActivity()));
        rvProduits.setAdapter(produitAdapter);

        nomMagasin = MagasinDetailFragmentArgs.fromBundle(getArguments()).getNomMagasin();

        ((MainActivity)getActivity()).getSupportActionBar().setTitle(nomMagasin);

        mediaPlayer = MediaPlayer.create(requireActivity(), R.raw.delete);

        prefs = getContext().getSharedPreferences(getString(R.string.sauvegarde), Context.MODE_PRIVATE );

        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int itemPosition = viewHolder.getAdapterPosition();
                if (mediaPlayer != null) {
                    mediaPlayer.start();
                }

                produitAdapter.notifyItemRemoved(itemPosition);

                db.collection(getString(R.string.collection_produits)).document(listProduits.get(itemPosition).getId()).delete();
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvProduits);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btAddProduit.setOnClickListener(this);



        CollectionReference produits = db.collection(getString(R.string.collection_produits));

        produits.whereEqualTo(getString(R.string.KEY_MAGASIN), nomMagasin)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        listProduits.clear();
                        for (QueryDocumentSnapshot document : value) {
                            Produit produit = document.toObject(Produit.class);
                            produit.setId(document.getId());
                            listProduits.add(produit);
                        }
                        produitAdapter.notifyDataSetChanged();
                    }
                });

    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null){
            mediaPlayer.pause();
            mediaPlayer.release();
        }
    }

    @Override
    public void onClick(View v) {
        String pseudo = prefs.getString(getString(R.string.pseudo),"");
        DialogFragment dialogFragment = new DialogProduit(nomMagasin,pseudo);
        dialogFragment.show(requireActivity().getSupportFragmentManager(),getString(R.string.ajouter_produit));
    }
}