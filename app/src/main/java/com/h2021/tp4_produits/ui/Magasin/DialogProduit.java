package com.h2021.tp4_produits.ui.Magasin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.h2021.tp4_produits.R;
import com.h2021.tp4_produits.model.Produit;

public class DialogProduit extends DialogFragment {

    private EditText etNomProduit, etPrixProduit;
    private String nomMagasin;
    private String  pseudo;
    private final String TAG = "tag";

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    public DialogProduit(String nomMagasin, String pseudo) {
        this.nomMagasin = nomMagasin;
        this.pseudo = pseudo;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.add_produit, null);

        etNomProduit = view.findViewById(R.id.et_nom_produit);
        etPrixProduit = view.findViewById(R.id.et_prix_produit);

        builder.setView(view)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String nomProduit  = etNomProduit.getText().toString().trim();
                        String prixProduit  = etPrixProduit.getText().toString().trim();

                        if (nomProduit.isEmpty()) {
                            Toast.makeText(getContext(), R.string.erreur_nom_magasin, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (prixProduit.isEmpty()) {
                            Toast.makeText(getContext(), R.string.prix_produit_erreur, Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            try {
                                double prix = Double.parseDouble(prixProduit);
                            } catch (NumberFormatException e) {
                                Toast.makeText(getContext(), R.string.erreur_prix, Toast.LENGTH_LONG).show();
                                return;
                            }
                        }

                        Produit produit = new Produit(nomProduit,Double.parseDouble(prixProduit),pseudo,nomMagasin);

                        db.collection(getString(R.string.collection_produits))
                                .add(produit)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        //Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference);
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //Log.w(TAG, "Error adding document", e);
                                    }
                                });

                    }
                })
                .setNegativeButton(R.string.annuler, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        builder.setTitle(R.string.ajouter_produit);

        return builder.create();
    }
}
