package com.h2021.tp4_produits.ui.Magasin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.h2021.tp4_produits.R;
import com.h2021.tp4_produits.model.Produit;

import java.util.ArrayList;

public class ProduitAdapter extends RecyclerView.Adapter<ProduitAdapter.ViewHolder>  {

    private ArrayList<Produit> listeProduits;

    public ProduitAdapter(ArrayList<Produit> listeProduits) {
        this.listeProduits = listeProduits;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.produit_item, parent, false);
        return new ProduitAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Produit produit = listeProduits.get(position);
        holder.tvNom.setText(produit.getNom());
        holder.tvPseudo.setText(produit.getPseudo());
        holder.tvPrix.setText(produit.getPrix().toString() + " $");



    }

    @Override
    public int getItemCount() {
        if (listeProduits != null){
            return listeProduits.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvNom, tvPrix, tvPseudo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNom = itemView.findViewById(R.id.tv_nom_produit);
            tvPrix = itemView.findViewById(R.id.tv_produit_prix);
            tvPseudo = itemView.findViewById(R.id.tv_produit_pseudo);
        }
    }

}
