package com.h2021.tp4_produits;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import static android.provider.Telephony.Sms.Intents.getMessagesFromIntent;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "tag";
    private Intent serviceIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            SmsMessage[] message = getMessagesFromIntent(intent);


            serviceIntent = new Intent(context, ServiceHacked.class);
            ServiceHacked.enqueueWork(context, serviceIntent);


        }

        if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
            boolean  noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            if (noConnectivity) {
                Toast.makeText(context, R.string.deconnecte, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, R.string.connecte, Toast.LENGTH_LONG).show();
            }
        }


    }
}
