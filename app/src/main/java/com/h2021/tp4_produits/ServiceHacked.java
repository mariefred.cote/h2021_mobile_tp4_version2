package com.h2021.tp4_produits;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.h2021.tp4_produits.model.Magasin;

import java.util.Random;

public class ServiceHacked extends JobIntentService {

    private Double minLat = 46.7;
    private Double maxLat = 46.8;
    private Double minLon = -71.30;
    private Double maxLon = -71.20;
    static final String TAG = "tag";

    FirebaseFirestore db = FirebaseFirestore.getInstance();



    static void enqueueWork(Context context, Intent work){
        enqueueWork(context, ServiceHacked.class, 123, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        Random ran = new Random();
        for (int i = 0; i < 10; i ++) {

            Double lat = minLat + (maxLat - minLat) * ran.nextDouble();
            Double lon = minLon + (maxLon - minLon) * ran.nextDouble();
            Magasin magasin = new Magasin(getString(R.string.hacked),lat,lon);
            db.collection(getString(R.string.collection_magasins))
                    .add(magasin)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            //Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //Log.w(TAG, "Error adding document", e);
                        }
                    });

            if (isStopped()) return;


            SystemClock.sleep(1000);
        }
    }
}