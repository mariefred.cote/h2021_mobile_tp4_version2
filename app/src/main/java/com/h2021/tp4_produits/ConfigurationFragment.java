package com.h2021.tp4_produits;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class ConfigurationFragment extends Fragment {

    private EditText etPseudo;
    private Button btModifPseudo;
    private String pseudo;
    private SharedPreferences prefs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_configuration, container, false);

        prefs = getContext().getSharedPreferences(getString(R.string.sauvegarde), Context.MODE_PRIVATE );
        pseudo = prefs.getString(getString(R.string.pseudo),"");

        etPseudo = view.findViewById(R.id.et_pseudo);
        btModifPseudo = view.findViewById(R.id.bt_modifPseudo);

        etPseudo.setText(pseudo);

        btModifPseudo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                String nouvPseudo = etPseudo.getText().toString().trim();
                if (!nouvPseudo.isEmpty()) {
                    pseudo = nouvPseudo;
                    editor.putString(getString(R.string.pseudo), nouvPseudo);
                    editor.apply();
                } else {
                    Toast.makeText(getContext(), getString(R.string.erreur_pseudo), Toast.LENGTH_LONG).show();
                    etPseudo.setText(pseudo);
                }
            }
        });

        return view;
    }
}
